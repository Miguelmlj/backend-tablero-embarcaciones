import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import path  from 'path';

import config from './config'
import Embarcaciones from "./routes/embarcaciones.routes";

const app = express()

app.set('port', config.port)

// Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//RUTAS PARA TRABAJAR EN SERVIDOR 

//RUTAS PARA TRABAJAR EN MODO DESARROLLO
app.use("/api", Embarcaciones);

app.use(express.static(path.join(__dirname, 'public')));

app.get( '*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public/index.html'))
})

export default app
import { getConnection, querys, sql } from "../database"

export const getEmbarcaciones = async (req, res) => {
    try {
        const pool = await getConnection();
        const result = await pool.request()
            .input("Empresa", sql.Int, req.body.Empresa)
            .input("Sucursal", sql.Int, req.body.Sucursal)
            .query(querys.getTableroEmbarcaciones)

        res.json(result.recordset)

    } catch (error) {
        res.status(500);
        res.send(error.message);
        console.log(error.message);
    }
}
export const querys = {
    // getTableroEmbarcaciones: `SELECT * FROM [Marina].[dbo].[Slips]`

	/* getTableroEmbarcaciones: `SELECT 
	Empresa, 
	Sucursal, 
	Slip, 
	Embarcacion, 
	Pies,
	Longitud,
	Observacion,
	Compartido, 
	Nombre_barco =(SELECT CASE when sli.Embarcacion <> 0 then  
	isnull((
	SELECT TOP(1) Nombre_barco 
	FROM [Marina].[dbo].[Embarcaciones] as emb 
	WHERE sli.Empresa = emb.Empresa AND sli.Sucursal=emb.Sucursal AND sli.Embarcacion=emb.Numero
	),'')  else '' end)
	,
	Tipo =(SELECT CASE when sli.Embarcacion <> 0 then  
	isnull((
	SELECT TOP(1) Tipo 
	FROM [Marina].[dbo].[Embarcaciones] as emb 
	WHERE sli.Empresa = emb.Empresa AND sli.Sucursal=emb.Sucursal AND sli.Embarcacion=emb.Numero
	),'')  else '' end)
	FROM [Marina].[dbo].[Slips] as sli WHERE Empresa=@Empresa AND Sucursal=@Sucursal AND Slip >=100 AND Slip < 900` */
	
	getTableroEmbarcaciones: `SELECT 
	Empresa, 
	Sucursal, 
	Slip, 
	Embarcacion, 
	Pies,
	Longitud =(SELECT CASE when sli.Embarcacion <> 0 then  
		isnull((
		SELECT TOP(1) Longitud 
		FROM [Marina].[dbo].[Embarcaciones] as emb 
		WHERE sli.Empresa = emb.Empresa AND sli.Sucursal=emb.Sucursal AND sli.Embarcacion=emb.Numero
		),0)  else 0 end)
	,
	Observacion,
	Compartido, 
	Nombre_barco =(SELECT CASE when sli.Embarcacion <> 0 then  
		isnull((
		SELECT TOP(1) Nombre_barco 
		FROM [Marina].[dbo].[Embarcaciones] as emb 
		WHERE sli.Empresa = emb.Empresa AND sli.Sucursal=emb.Sucursal AND sli.Embarcacion=emb.Numero
		),'')  else '' end)
	,
	Tipo =(SELECT CASE when sli.Embarcacion <> 0 then  
		isnull((
		SELECT TOP(1) Tipo 
		FROM [Marina].[dbo].[Embarcaciones] as emb 
		WHERE sli.Empresa = emb.Empresa AND sli.Sucursal=emb.Sucursal AND sli.Embarcacion=emb.Numero
		),'')  else '' end)
	FROM [Marina].[dbo].[Slips] as sli WHERE Empresa=@Empresa AND Sucursal=@Sucursal AND Slip >=100 AND Slip < 900`

}
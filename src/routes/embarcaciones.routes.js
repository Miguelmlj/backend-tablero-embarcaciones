import { Router } from "express";
import {
    getEmbarcaciones
} from "../controllers/embarcaciones.controller";

const router = Router();

router.post("/embarcaciones/get", getEmbarcaciones);

export default router;